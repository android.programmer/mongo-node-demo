FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y --no-install-recommends npm \
    && npm i mongodb 
# RUN npm install -g npm@7.3.0
# RUN npm i mongodb

ADD app.js /app.js

RUN chmod +x /app.js

# RUN sleep 10

# RUN node /app.js
CMD ["node /app.js"]